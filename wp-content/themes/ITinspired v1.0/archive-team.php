<?php

?>

<?php get_header(); ?>
	
	<header class="article-header" id="squad-header">
		<div class="row">
			<div class="large-6 medium-6 medium-push-6 columns"> 
				<p class="text-center"><img src="<?php echo get_theme_file_uri( '/assets/images/squad-icon@2x.png' ); ?>" alt="<?php the_title(); ?>" width="65%"></p>
			</div>
			<div class="large-6 medium-6 medium-pull-6 columns">
				<h2 class="page-subtitle">Get to Know Our</h2>
				<h1 class="page-title">Squad</h1>
				<p class="light">The expertise you need, and the people you’ll love. We are a team of qualified, personable and excited IT professionals ready to help your business grow. We are your technology people!</p>
			<!-- 	<a href="/careers/" class="button success large">Join Our Squad</a> -->
			</div>
		</div>
	</header> <!-- end article header -->
			
	<div id="content">
		<div id="inner-content" class="row expanded">
			 <main id="main" class="large-12 medium-12 columns" role="main">

		       	<div id="modal-grid" class="hide-for-large">
		       		<?php if (have_posts()) : ?>
						<?php /* The loop */ ?>
			            
						 <?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'parts/loop', 'archive-team-mobile' ); ?> 
						<?php endwhile; ?> 
					<?php endif; ?>  
		       	</div>

		       	
		        <ul id="og-grid" class="og-grid show-for-large">            
					<?php if (have_posts()) : ?>
						<?php /* The loop */ ?>
			            
						 <?php while ( have_posts() ) : the_post(); ?>
							<?php get_template_part( 'parts/loop', 'archive-team' ); ?> 
						<?php endwhile; ?> 
					<?php endif; ?>        
		       	</ul>
	       	

	        
				<?php joints_page_navi(); ?>

			
	            
			

			</main>
		</div><!-- #inner-content -->

		<div class="home-squad-container">
			<div class="row">
				<div class="large-6 medium-6 small-12 columns">
					<section class="your-face-img featured-image" itemprop="articleBody">
						<img src="<?php echo get_theme_file_uri( '/assets/images/your-face-here@2x.png' ); ?>" class="attachment-full size-full wp-post-image">		
					</section>
				</div>
				<div class="large-6 medium-6 small-12 columns what-inspires-you">
					<h4>What Inspires You?</h4>
				
					<p class="lead">Are you ready to bring big solutions to the table and constantly challenge the perception of IT, all with a smile on your face? Join our team and become one of the coolest techies around!</p>
					<a href="/careers" class="button hollow white">Explore Career Opportunities</a>
				</div>
			</div>
		</div>	

	</div><!-- #content -->



<?php get_footer(); ?>
