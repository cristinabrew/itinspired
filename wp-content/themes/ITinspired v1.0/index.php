<?php get_header(); ?>

	<header id="blog-header">
		<div class="row">
			<div class="large-6 medium-6 medium-push-6 columns"> 
				<p class="text-center"><img src="<?php echo get_theme_file_uri( '/assets/images/blog-icon@2x.png' ); ?>" alt="ITinspired Blog" width="55%"></p>
			</div>
			<div class="large-6 medium-6 medium-pull-6 columns">
				<h2 class="page-subtitle">Latest Happenings</h2>
				<h1 class="page-title">Inspired Blog</h1>
				<p class="lead light"></p>
			</div>

		</div>
	</header> <!-- end article header -->
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns blog-archive" role="main">
		    
			    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			 
					<!-- To see additional archive styles, visit the /parts directory -->
					<?php get_template_part( 'parts/loop', 'archive-grid' ); ?>
				    
				<?php endwhile; ?>	

					<?php joints_page_navi(); ?>
					
				<?php else : ?>
											
					<?php get_template_part( 'parts/content', 'missing' ); ?>
						
				<?php endif; ?>
																								
		    </main> <!-- end #main -->
		    

		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>