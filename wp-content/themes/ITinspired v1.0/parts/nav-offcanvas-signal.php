<!-- This is used in addition to the main navigation menu, to add the Signal Us floating toggle -->
<div class="data-sticky-container show-for-large">
	<div class="signal-offcanvas" data-sticky-container>
		<div id="signal-toggle-container" data-sticky data-options="marginTop:0;stickTo:top;stickyOn:small;" >
				<button class="signal-icon" type="button" data-toggle="signal-canvas">
					<img class="closed" src="<?php echo get_theme_file_uri( '/assets/images/lightbulb-icon@2x.png' ); ?>">
					<img class="open" src="<?php echo get_theme_file_uri( '/assets/images/lightbulb-icon-yellow@2x.png' ); ?>">
				</button>
				
		</div>
	</div>
</div>