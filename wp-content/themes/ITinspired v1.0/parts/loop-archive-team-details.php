<?php 
				//vars
				$post = get_the_id();
				$altimg = get_field( 'alternate_image', $post );
				$funimg = get_field( 'fun_image', $post );
				$title = get_field( 'title', $post );
				$certs = get_field( 'certifications', $post );
				$perstalents = get_field( 'personal_talents', $post );
				$protalents = get_field( 'professional_talents', $post );
				$song = get_field( 'favorite_song', $post );
				$superhero = get_field( 'superhero', $post );
				$kids = get_field( 'kids_pets', $post );
				$vacation = get_field( 'vacation_spot', $post );

				?>

<div class="toggled-row toggled-<?php echo $post; ?> row">
	
			<div class="row">
				<div class="small-12 large-4 columns">
					<img src="<?php echo $funimg; ?>">
				</div>
				<div class="small-12 large-8 columns">
					<div class="row">
						<h3><?php the_title(); ?>&nbsp;<span><?php echo $title; ?></span></h3>
						<h5><?php echo $certs; ?></h5>
						<p><?php the_content(); ?></p>
					</div>
					<div class="row">
						<div class="small-12 large-6 columns">
							<h6>Talents :: Personal</h6>
							<p><?php echo $perstalents; ?></p>
							<h6>Favorite Song</h6>
							<p><?php echo $song; ?></p>
							<h6>Kids &amp; Pets</h6>
							<p><?php echo $kids; ?></p>
						</div>
						<div class="small-12 large-6 columns">
							<h6>Talents :: Professional</h6>
							<p><?php echo $protalents; ?></p>
							<h6>Superhero Persona</h6>
							<p><?php echo $superhero; ?></p>
							<h6>Favorite Vacation Spot</h6>
							<p><?php echo $vacation; ?></p>
						</div>
					</div>
				</div>
			</div>

</div>