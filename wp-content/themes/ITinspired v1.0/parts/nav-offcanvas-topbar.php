<!-- By default, this menu will use off-canvas for small
	 and a topbar for medium-up -->
<div data-sticky-container>
	<div id="top-bar-main" data-sticky-container> 
		<div class="top-bar" id="top-bar-menu" data-sticky data-options="marginTop:0;stickTo:top;stickyOn:small;" style="width:100%">
			<div class="top-bar-left float-left">
				<ul class="menu">
					<li class="show-for-medium"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_theme_file_uri( '/assets/images/itinspired-logo.svg' ); ?>" class="logo"></a></li>
					<li class="show-for-small-only"><a href="<?php echo home_url(); ?>"><img src="<?php echo get_theme_file_uri( '/assets/images/itinspired-logo-lt.svg' ); ?>" class="logo"></a></li>
				</ul>
			</div>
		<!-- 	<div class="top-bar-right show-for-medium">
				<?php joints_top_nav(); ?>	
			</div> -->
			<div class="top-bar-right float-right show-for-small">
				<ul class="menu">
					<li><a href="/get-in-touch/" class="button hollow contact-button show-for-medium">Get in Touch</a></li>
					<!-- <li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li> -->
					<li><button class="menu-sb" type="button" data-toggle="off-canvas"><img src="<?php echo get_theme_file_uri( '/assets/images/starburst-menu@2x.png' ); ?>" width="60px"></button></li>

						<!-- <li><div class="menu-icon-bg"><span><span><span><button class="menu-icon" type="button" data-toggle="off-canvas"></button></span></span></span></div></li> -->
				</ul>
			</div>
		</div>
	</div>
</div>