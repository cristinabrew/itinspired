<?php 
//acf vars
	$certs = get_field( 'certifications' );
	$perstalents = get_field( 'personal_talents' );
	$protalents = get_field( 'professional_talents' );
	$altimg = get_field( 'alternate_image' );
?>
			
		<div class="row" data-equalizer data-equalize-on="medium">
			<div class="large-6 small-12 columns" data-equalizer-watch>
					<section class="home-team-img featured-image" itemprop="articleBody">
						<?php the_post_thumbnail( 'full'); ?>					
					</section>
			</div>
			<div class="large-6 small-12 columns" data-equalizer-watch>
				<h4>Featured Squad Member</h4>


					<h3><?php the_title(); ?></h3>
					<h6></h6>

					<div class="orbit" role="region" aria-label="Squad Member Details" data-orbit>
						<ul class="orbit-container">
							<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span> &#9664;&#xFE0E;</button>
							<button class="orbit-next"><span class="show-for-sr">Next Slide</span> &#9654;&#xFE0E;</button>			
							 
							 <li class="orbit-slide is-active">
							 	<h6>Personal Talents:</h6>
							    <p class="text-center"><?php echo $perstalents; ?></p>
							 </li>
							 <li class="orbit-slide">
							 	<h6>Professional Talents:</h6>
							 	<p class="text-center"><?php echo $protalents; ?></p>
							 </li>
							 <li class="orbit-slide">
							 	<h6>Certified in:</h6>
							    <p class="text-center"><?php echo $certs; ?></p>
							 </li>
						</ul>
								
					</div>
						
				<p class="lead">The <span>coolest</span> techies you'll ever meet!</p>
				<a href="/squad/" class="button hollow white">View All</a>
			</div>
		</div> 	