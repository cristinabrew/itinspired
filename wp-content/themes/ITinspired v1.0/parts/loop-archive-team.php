<?php 
				//vars
				$post = get_the_id();
				$altimg = get_field( 'alternate_image', $post );
				$funimg = get_field( 'fun_image', $post );
				$title = get_field( 'title', $post );
				$certs = get_field( 'certifications', $post );
				?>

 <?php
//Preview Size Image
 if ( has_post_thumbnail() ) {
  $image_id = get_post_thumbnail_id();	 
  $image_url = wp_get_attachment_image_src($image_id,'homepage-preview');
  $url = $image_url[0];
 }
 else $url = get_bloginfo('stylesheet_directory').'/images/home-preview-default.jpg'
 ?>
 
  <?php
//Thumb Size Image
 if ( has_post_thumbnail() ) {
  $image_id = get_post_thumbnail_id();	 
  $image_url = wp_get_attachment_image_src($image_id,'homepage-preview');
  $url2 = $image_url[0];
 }
 else $url2 = get_bloginfo('stylesheet_directory').'/images/home-thumb-default.jpg'
 ?>


<li><!--post-->
	<a data-largesrc="<?php echo $funimg; ?>" data-title="<?php the_title(); ?>&nbsp;<span><?php echo $title; ?></span>" data-certificates="<?php echo $certs; ?>" data-description="<?php the_content(); ?>">
		<section class="team-img featured-image">
		<img src="<?php echo $url2; ?>" alt="<?php the_title(); ?>">
		<img src="<?php echo $altimg; ?>">
		</section>
		<h3><?php the_title(); ?></h3>
	</a>
</li><!--end of post-->