<!-- ON MOBILE ONLY -->
<?php 
// Adjust the amount of rows in the grid
$grid_columns = 3; ?>

<?php if( 0 === ( $wp_query->current_post  )  % $grid_columns ): ?>

<div class="row archive-grid" data-equalizer> <!--Begin Row:--> 

<?php endif; ?> 

				<!--Item: -->

				<?php 
				//vars
				$post = get_the_id();
				$altimg = get_field( 'alternate_image', $post );
				$funimg = get_field( 'fun_image', $post );
				$title = get_field( 'title', $post );
				$certs = get_field( 'certifications', $post );
				$perstalents = get_field( 'personal_talents', $post );
				$protalents = get_field( 'professional_talents', $post );
				$song = get_field( 'favorite_song', $post );
				$superhero = get_field( 'superhero', $post );
				$kids = get_field( 'kids_pets', $post );
				$vacation = get_field( 'vacation_spot', $post );

				?>

				<div class="large-4 medium-4 columns panel" data-equalizer-watch data-aos="fade-up">
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">

						<header class="team-header" data-open="popup-<?php echo $post; ?>">

							<section class="team-img featured-image" itemprop="articleBody">					
								<?php the_post_thumbnail('full'); ?>
								<img src="<?php echo $altimg; ?>">
							</section> <!-- end article section -->

							
							<h3><?php the_title(); ?></h3>
							<button class="popup-button">&plus;</button>
						</header> <!-- end article header -->


						<div class="large reveal team-details" id="popup-<?php echo $post; ?>" data-reveal data-animation-in="fade-in" data-animation-out="fade-out">	
							  <button class="close-button" data-close aria-label="Close Details" type="button">
							    <span aria-hidden="true">&times;</span>
							  </button>
							<div class="row">
								<div class="small-12 large-4 columns">
									<img src="<?php echo $funimg; ?>">
								</div>
								<div class="small-12 large-8 columns">
									<div class="row">
										<h3><?php the_title(); ?>&nbsp;<span><?php echo $title; ?></span></h3>
										<h5><?php echo $certs; ?></h5>
										<p class="lead"><?php the_content(); ?></p>
									</div>
									<!-- <div class="row">
										<div class="small-12 large-6 columns">
											<h6>Talents :: Personal</h6>
											<p><?php echo $perstalents; ?></p>
											<h6>Favorite Song</h6>
											<p><?php echo $song; ?></p>
											<h6>Family &amp; Pets</h6>
											<p><?php echo $kids; ?></p>
										</div>
										<div class="small-12 large-6 columns">
											<h6>Talents :: Professional</h6>
											<p><?php echo $protalents; ?></p>
											<h6>Superhero Persona</h6>
											<p><?php echo $superhero; ?></p>
											<h6>Favorite Vacation Spot</h6>
											<p><?php echo $vacation; ?></p>
										</div>
									</div> -->
								</div>
							</div>
						</div>
										    							
					</article> <!-- end article -->
					
				</div>

<?php if( 0 === ( $wp_query->current_post + 1 )  % $grid_columns ||  ( $wp_query->current_post + 1 ) ===  $wp_query->post_count ): ?>

	</div>  <!--End Row: --> 

<?php endif; ?>