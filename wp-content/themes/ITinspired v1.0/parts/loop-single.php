<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
	
					
    <section class="single-featured-img">
		<?php the_post_thumbnail('full'); ?>	
	</section> <!-- end article section -->

	<header class="single-article-header">	
		<h1 class="entry-title single-title" itemprop="headline"><?php the_title(); ?></h1>
		<?php get_template_part( 'parts/content', 'byline' ); ?>
    </header> <!-- end article header -->

    <section id="blog-content" class="entry-content" itemprop="articleBody">
    	<?php the_content(); ?>
    </section>

	<footer class="article-footer">
		<?php wp_link_pages( array( 'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'jointswp' ), 'after'  => '</div>' ) ); ?>
		<p class="tags"><?php the_tags('<span class="tags-title">' . __( 'Tags:', 'jointswp' ) . '</span> ', ', ', ''); ?></p>	
		
	</footer> <!-- end article footer -->

	<?php joints_related_posts(); ?>
	
	<section id="home-newsletter">
		<h4>Want to Stay Connected with ITinspired?</h4>
		<div class="row signup-container">
		<?php
		if( function_exists( 'mc4wp_show_form' ) ) {
		mc4wp_show_form( 168 );
		};
		?>
		</div>
	</section>
													
</article> <!-- end article -->

