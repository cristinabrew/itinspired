<?php 
// Adjust the amount of rows in the grid
$grid_columns = 3; ?>

<?php if( 0 === ( $wp_query->current_post  )  % $grid_columns ): ?>

    <div class="row archive-grid blog-archive-grid" data-equalizer> <!--Begin Row:--> 

<?php endif; ?> 

		<!--Item: -->
		<div class="large-4 medium-4 small-12 columns panel" data-equalizer-watch data-aos="fade-up">
		
			<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
			
				<div class="featured-image" itemprop="articleBody">
					<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('full', array('class' => 'image-hover')); ?></a>
				</div> <!-- end article section -->
			
				<header class="article-header-feed">
					<h5 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>	
							<?php get_template_part( 'parts/content', 'byline-date' ); ?>						
				</header> <!-- end article header -->												
								    							
			</article> <!-- end article -->
			
		</div>

<?php if( 0 === ( $wp_query->current_post + 1 )  % $grid_columns ||  ( $wp_query->current_post + 1 ) ===  $wp_query->post_count ): ?>

   </div>  <!--End Row: --> 

<?php endif; ?>

