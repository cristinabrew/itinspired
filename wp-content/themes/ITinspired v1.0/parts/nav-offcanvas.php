<div class="top-bar" id="top-bar-menu">
	<div class="top-bar-left">
		<ul class="menu">
				<li><a href="<?php echo home_url(); ?>"><img src="<?php echo get_theme_file_uri( '/assets/images/itinspired-logo.svg' ); ?>" class="logo"></a></li>
		</ul>
	</div>
	<div class="top-bar-right">
		<ul class="menu">
			<li><a data-toggle="off-canvas"><?php _e( 'Menu', 'jointswp' ); ?></a></li>
			<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
		</ul>
	</div>
</div>