<div class="off-canvas position-left" id="signal-canvas" data-off-canvas data-transition="overlap">
	
	<div class="row">
		<button class="close-button" data-close aria-label="Close Signal Us" type="button">
		    <span aria-hidden="true">&times;</span>
		</button>
		<h2>Signal Us</h2>
		<section class="lightbulb-icon" itemprop="articleBody">
				<img src="<?php echo get_theme_file_uri( '/assets/images/lightbulb-icon@2x.png' ); ?>" width="40px">
				<img src="<?php echo get_theme_file_uri( '/assets/images/lightbulb-icon-yellow@2x.png' ); ?>" width="40px"/>
		</section>
		<p class="intro">Signal us between 8­–5pm, Monday–Friday, for fast, dedicated support. If you send us a message after hours, we’ll get back to you as soon as possible, usually within 24 business hours.
		</p>


		<h4>Call <span>225 / 424 / 8000</span></h4>
		<h4>Or Send us a message</h4>
		<?php echo do_shortcode( '[contact-form-7 id="4" title="Signal Us Off-Canvas"]' ); ?>
	</div>

</div>