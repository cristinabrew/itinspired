<div class="off-canvas position-right" id="off-canvas" data-off-canvas data-transition="overlap">
	<button class="close-button" data-close aria-label="Close" type="button">
	    <span aria-hidden="true">&times;</span>
	</button>
	<div class="off-canvas-padding">
		<?php joints_off_canvas_nav(); ?>
	</div>
	<div id="off-canvas-contact">
		<ul >
			<li><a href="http://www.facebook.com/ITinspired" target="_blank"><img src="<?php echo get_theme_file_uri( '/assets/images/facebook-sb@2x.png' ); ?>"></a></li>
			<li><a href="http://www.linkedin.com/company/itinspired" target="_blank"><img src="<?php echo get_theme_file_uri( '/assets/images/linkedin-sb@2x.png' ); ?>"></a></li>
			<li><a href="http://www.instagram.com/ITinspiredBR" target="_blank"><img src="<?php echo get_theme_file_uri( '/assets/images/instagram-sb@2x.png' ); ?>"></a></li>
		</ul>	
		<div class="contact-details">
			<div class="row small-uncollapse medium-collapse">
				<div class="small-12 medium-6 columns">
					<a href="mailto:support@itinspired.com" target="_blank"><img class="email-img" src="<?php echo get_theme_file_uri( '/assets/images/email@2x.png' ); ?>">support@ITinspired.com</a>
				</div>
				<div class="small-12 medium-6 columns">
					<a href="tel:12254248000" target="_blank"><img class="tel-img" src="<?php echo get_theme_file_uri( '/assets/images/phone@2x.png' ); ?>">225 / 424 / 8000</a>
				</div>
				<div class="small-12 medium-12 columns">
					<p>your technology people</p>
				</div>
			</div>
			
		</div>
	</div>
</div>