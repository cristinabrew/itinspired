<?php
/*
Template Name: Solutions
*/
?>

<?php get_header(); ?>
	
	<header class="article-header" style="background-image: url(<?php the_field('background_image'); ?>);">
		<div class="row">
			<div class="small-12 medium-6 medium-push-6 columns"> 
				<p class="text-center"><img src="<?php the_field('header_icon'); ?>" alt="<?php the_title(); ?>" width="65%"></p>
			</div>
			<div class="small-12 medium-6 medium-pull-6 columns">
				<h2 class="page-subtitle"><?php the_field('page_subtitle'); ?></h2>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php if(get_field('header_intro'))
				{
					echo get_field('header_intro') ;
				}

				?>
			</div>

		</div>
	</header> <!-- end article header -->
			
	<div id="content">
	
	<!-- 	<div id="inner-solutions-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main"> -->
				
		    	<div class="solutions-content">
		    		<h2>Pick a solution:</h2>
		    		<div class="row solutions-icons">

					    	<div class="small-4 columns" data-aos="fade-up">
								<img class="popup-toggle" data-popup="popup-1" src="<?php echo get_theme_file_uri( '/assets/images/data-icon@2x.png' ); ?>"><br/>
								<!-- <button class="popup-button" data-popup="popup-1">&plus;</button> -->
							</div>
							<div class="small-4 columns" data-aos="fade-up">
								<img class="popup-toggle" data-popup="popup-2" src="<?php echo get_theme_file_uri( '/assets/images/cloud-icon@2x.png' ); ?>"><br/>
								<!-- <button class="popup-button" data-popup="popup-2">&plus;</button> -->
							</div>
							<div class="small-4 columns" data-aos="fade-up">
								<img class="popup-toggle" data-popup="popup-3" src="<?php echo get_theme_file_uri( '/assets/images/voice-icon@2x.png' ); ?>"><br/>
								<!-- <button class="popup-button" data-popup="popup-3">&plus;</button> -->
							</div>
		    		
		    		</div>

		    		<div class="row">
		    		<div class="callout popup hide" id="popup-1">
		    			<?php if( have_rows('data_bucket') ): ?>
		    				<div class="row small-up-1 medium-up-2 large-up-2">
		    					<h2>Data</h2>
		    			 		<?php while( have_rows('data_bucket') ): the_row(); 
						        
						        // vars
						        $title = get_sub_field('data_title'); 
						        $description = get_sub_field('data_description'); 			       		
						        ?>						    						        
						        
						        	<div class="column column-block" data-aos="fade-up"> 
									<h4><?php echo $title; ?></h4>
									<p><?php echo $description; ?></p>
									</div>
									
								<?php endwhile; ?>									
			    			</div>	
			    		<?php endif; ?>
			    		<button class="close-button popup-close-button" type="button">&times;</button>
		    		</div> 


		    		<div class="callout popup hide" id="popup-2">
		    			<?php if( have_rows('cloud_bucket') ): ?>
		    				<div class="row small-up-1 medium-up-2 large-up-2">
		    					<h2>Cloud</h2>
		    			 		<?php while( have_rows('cloud_bucket') ): the_row(); 
						        
						        // vars
						        $title = get_sub_field('cloud_title'); 
						        $description = get_sub_field('cloud_description'); 			       
										
						        ?>						    						        
						        
						        	<div class="column column-block" data-aos="fade-up"> 
									<h4><?php echo $title; ?></h4>
									<p><?php echo $description; ?></p>
									</div>
									
								<?php endwhile; ?>									
			    			</div>	
			    		<?php endif; ?>
			    		<button class="close-button popup-close-button" type="button">&times;</button>
		    		</div> 


		    		<div class="callout popup hide" id="popup-3">
		    			<?php if( have_rows('voice_bucket') ): ?>
		    				<div class="row small-up-1 medium-up-2 large-up-2">
		    					<h2>Voice</h2>
		    			 		<?php while( have_rows('voice_bucket') ): the_row(); 
						        
						        // vars
						        $title = get_sub_field('voice_title'); 
						        $description = get_sub_field('voice_description'); 			       
										
						        ?>						    						        
						        
						        	<div class="column column-block" data-aos="fade-up"> 
									<h4><?php echo $title; ?></h4>
									<p><?php echo $description; ?></p>
									</div>
									
								<?php endwhile; ?>									
			    			</div>	
			    		<?php endif; ?>
			    		<button class="close-button popup-close-button" type="button">&times;</button>
		    		</div> 

			    	</div>

		    	</div> <!-- End Solutions Content -->


		    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<p class="lead"><?php get_template_part( 'parts/loop', 'page' ); ?></p>
							
				<?php endwhile; endif; ?>


				<div id="partners" class="row">
				    <div class="large-12 medium-12 columns">
					    <h5>The Company We Keep</h5>
					    <h3>Partners</h3>

					    <?php if( have_rows('partner') ): ?>

					    <div class="row small-up-2 medium-up-5 large-up-5" data-aos="fade-up">
						    <?php while( have_rows('partner') ): the_row(); 
							    
						        
						        // vars
						        $plogo = get_sub_field('partner_logo'); 
						        $ptitle = get_sub_field('partner_title'); 
						        $pdescription = get_sub_field('partner_description'); 
						        
						        ?>

							    <div class="column column-block"> 
							    	
							    	

							    	<ul class="accordion" data-accordion data-allow-all-closed="true">
							    		<li class="accordion-item" data-accordion-item>
											<a href="#" class="accordion-title"><img class="partner-logo" src="<?php echo $plogo; ?>" alt="<?php echo $ptitle; ?>">
											</a>
							    		<div class="accordion-content" data-tab-content>
												<p><?php echo $pdescription; ?></p>  
										</div>
										</li>
							    	</ul>

									
							    </div>
						    <?php endwhile; ?>
						 
						</div>
						 
						<?php endif; ?>
					</div>
				</div>
				

				<div class="testimonials solutions-footer">
					<div class="row">
					<?php get_sidebar('testimonialsb'); ?>
					</div>
				</div>	


			<!-- </main> --> <!-- end #main -->
		    
	<!-- 	</div>  --><!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
