<?php
/*
Template Name: Homepage
*/
?>

<?php get_header(); ?>

	<div id="hero-container">
		<div class="animation-parent">
			<div id="bodymovin"></div>
			<!-- <div id="hero-spacer">
			</div>

			<div class="hero-animation">			
			</div> -->
				
		</div>
		<div class="hero-buttons">
				<a href="/solutions/#data" class="button small success data-button"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/data@2x.png' ); ?>">Data</a>
				<a href="/solutions/#cloud" class="button small success cloud-button"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/cloud@2x.png' ); ?>">Cloud</a>
				<a href="/solutions/#voice" class="button small success voice-button"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/voice@2x.png' ); ?>">Voice</a>
		</div>
		<div class="row hero-headline">

			<h4 class="h4-hero">Serious technology. Inspiring people.</h4>
			<!-- <img src="<?php echo get_theme_file_uri( '/assets/images/mouse-scroll-icon@2x.png' ); ?>" height="50px"> -->
			<div class="mouse-scroll"><img src="<?php echo get_theme_file_uri( '/assets/images/mouse-scroll-icon@2x.png' ); ?>" height="50px"></div>
		</div>
	</div>

	<section id="home-content">

		<div class="row technology-people">
			<div class="large-6 large-push-6 columns" data-aos="fade-up">
				<img class="content-icon" src="<?php echo get_theme_file_uri( '/assets/images/lightbulb-icon-lt@2x.png' ); ?>">
			</div>
			<div class="large-6 large-pull-6 columns" data-aos="fade-up">
				<h3>your technology <span>people</span></h3>
				<p>We aren’t your typical IT company.<br/>
					We won’t back down from a challenge.<br/>
					We will leave you inspired.	</p>
					<a href="/about-us/" class="button hollow">Learn About Us</a>

			</div>
		</div>

		<div class="row hide-for-small show-for-large mid-div-line" data-aos="fade-up">
			<img src="<?php echo get_theme_file_uri( '/assets/images/line-2.png' ); ?>">
		</div>

		<div class="row technology-solutions">
			<div class="large-6 small-12 columns" data-aos="fade-up">
				<img class="content-icon" src="<?php echo get_theme_file_uri( '/assets/images/service-icon-row@2x.png' ); ?>">

			</div>
			<div class="large-6 small-12 columns" data-aos="fade-up">
				<h3>your technology <span>solutions</span></h3>
				<p>From consulting to regular monthly maintenance,<br/> 
					our team is here to provide the solutions you need,<br/>
					the efficiency you want, and the personality you’ll love.</p>
				<a href="/solutions/" class="button hollow">Learn About Our Solutions</a>
			</div>
		</div>

	</section>
	
	<section id="home-squad">

		<div class="home-squad-container">
			<?php 	
			remove_all_filters('posts_orderby');
			$team_args = array( 		
				'post_type' => 'team',  // The Custom Post Type named 'team' 		
				'orderby' => 'rand', // Display randomly 
				'posts_per_page' => 1,  // Get 1 post		
				);

			$rand_team_member = new WP_Query( $team_args );  	
			?>


			 <?php if ($rand_team_member->have_posts()) : while ($rand_team_member->have_posts()) : $rand_team_member->the_post(); ?>
					
					    	<?php get_template_part( 'parts/loop', 'random-team' ); ?>
					    	
					    <?php endwhile; wp_reset_postdata(); else : ?>
					
					   		<?php get_template_part( 'parts/content', 'missing' ); ?>

					    <?php endif; ?>	
		</div>

	</section>


	<section id="home-posts">

		<div class="row column">
			<h4>Recent Posts</h4>
				
			<div id="home-post-feed" class="row archive-grid" data-equalizer> <!--Begin Row:--> 
			
			<!--  Define our WP Query Parameters -->
			<?php $the_query = new WP_Query( 'posts_per_page=3' ); ?>

			<!-- Start our WP Query -->
			<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>

			<!--Item: -->
				<div class="large-4 medium-4 columns panel" data-equalizer-watch>
				
					<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
					
						<div class="featured-image" itemprop="articleBody">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('full', array('class' => 'image-hover')); ?></a>
						</div> <!-- end article section -->
					
						<header class="article-header-feed">
							<h5 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>	
							<?php get_template_part( 'parts/content', 'byline-date' ); ?>	

						</header> <!-- end article header -->	
										    							
					</article> <!-- end article -->
					
				</div>

			<!-- Repeat the process and reset once it hits the limit -->
			<?php 
			endwhile;
			wp_reset_postdata();
			?>
			</ul>
			</div>
				    
		
		</div>

	</section>

	<section id="home-newsletter" data-aos="fade-right">
		<div data-aos="fade-right"><h4>Want to Stay Connected with ITinspired?</h4></div>
		<div class="row signup-container">
		<?php
		if( function_exists( 'mc4wp_show_form' ) ) {
		mc4wp_show_form( 168 );
		};
		?>
		</div>
	</section>

	<div class="testimonials solutions-footer">
		<div class="row">
		<?php get_sidebar('testimonialsb'); ?>
		</div>
	</div>	

<?php get_footer(); ?>