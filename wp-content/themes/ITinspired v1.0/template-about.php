<?php
/*
Template Name: About Us
*/
?>

<?php get_header(); ?>
	
	<header class="article-header" style="background-image: url(<?php the_field('background_image'); ?>);">
		<div class="row">
			<div class="large-6 medium-6 medium-push-6 columns"> 
				<p class="text-center"><img src="<?php the_field('header_icon'); ?>" alt="<?php the_title(); ?>" width="65%"></p>
			</div>
			<div class="large-6 medium-6 medium-pull-6 columns">
				<h2 class="page-subtitle"><?php the_field('page_subtitle'); ?></h2>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php if(get_field('header_intro'))
				{
					echo get_field('header_intro');
				}

				?>
			</div>

		</div>
	</header> <!-- end article header -->
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">
				
				<div id="mission-vision" class="orbit" role="region" aria-label="Mission and Vision" data-orbit>
						<ul class="orbit-container">
							<button class="orbit-previous"><span class="show-for-sr">Previous Slide</span> &#9664;&#xFE0E;</button>
							<button class="orbit-next"><span class="show-for-sr">Next Slide</span> &#9654;&#xFE0E;</button>
						  <li class="orbit-slide is-active">
						  	<div class="row">
							    <div class="large-6 medium-6 columns">
								    <p class="text-center"><img src="<?php echo get_theme_file_uri( '/assets/images/mission-graphic@2x.png' ); ?>" alt="ITinspired Mission" width="200px"></p>
							    </div>
							    <div class="large-6 medium-6 columns">
								    <h5>What We Strive For</h5>
								    <h3>Mission</h3>
								    <p>Our mission is to put people first while implementing technology that positively impacts and inspires your business.</p>
								</div>
							</div>
						  </li>
						 <li class="orbit-slide">
						   <div class="row">
							    <div class="large-6 medium-6 columns">
								    <p class="text-center"><img src="<?php echo get_theme_file_uri( '/assets/images/vision-graphic@2x.png' ); ?>" alt="ITinspired Vision" width="200px"></p>
							    </div>
							    <div class="large-6 medium-6 columns">
								    <h5>Picture of the Preferred Future</h5>
								    <h3>Vision</h3>
								    <p>Our vision is for ITinspired to be the company that changes the perception of IT.</p>
								</div>
							</div>
						 </li>
						</ul>
				</div>

				<div id="culture" class="row">
					<div class="large-6 medium-6 medium-push-6 columns">
					    <p class="text-center"><img src="<?php echo get_theme_file_uri( '/assets/images/culture-graphic@2x.png' ); ?>" alt="ITinspired Culture" width="300px"></p>
				    </div>
				    <div class="large-6 medium-6 medium-pull-6 columns">
					    <h5>How We Operate</h5>
					    <h3>Culture</h3>
					    <p>Our corporate culture is nourished by a positive learning environment and fosters constant growth for our team members—both professionally and personally. We encourage forward thinking, passionate design, honesty, community involvement, and above all—inspiration!</p>
	
						<p>We’re people with passion and dedication, and our team is encouraged to act, think, and execute freely to get the job done. That’s how we deliver the best service possible to our clients!</p>
					</div>
					
				</div>
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
							
				<?php endwhile; endif; ?>

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->

				<div class="testimonials about-footer">
					<div class="row">

						<?php get_sidebar('testimonialsb'); ?>
					</div>
				</div>	

	
	</div> <!-- end #content -->

<?php get_footer(); ?>
