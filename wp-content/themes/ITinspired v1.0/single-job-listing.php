<?php
/*
Category Template: Careers
*/
?>

<?php get_header(); ?>

	<header class="article-header" style="background-image: url(<?php echo get_theme_file_uri( '/assets/images/careers-hero@2x.jpg' ); ?>);">
		<h1 class="page-title"><?php the_title(); ?></h1>
		<a href="/careers" class="button hollow white">Back to Careers Page</a>
	</header> <!-- end article header -->
			
	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-12 medium-12 columns" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<?php get_template_part( 'parts/loop', 'page' ); ?>
					
				<?php endwhile; endif; ?>							

			</main> <!-- end #main -->
		    
		</div> <!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>