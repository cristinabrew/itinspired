<?php get_header(); ?>

	<header class="article-header" style="background-image: url(<?php 
	if(get_field('background_image'))
		{
	echo get_field('background_image');
		} 
	else {
		echo get_theme_file_uri( '/assets/images/careers-hero@2x.jpg' );
	}
?>);">
		<div class="row">
			<div class="large-6 medium-6 medium-push-6 columns"> 
				<p class="text-center"><img src="<?php the_field('header_icon'); ?>" width="65%"></p>
			</div>
			<div class="large-6 medium-6 medium-pull-6 columns">
				<h2 class="page-subtitle"><?php the_field('page_subtitle'); ?></h2>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php if(get_field('header_intro'))
				{
					echo get_field('header_intro') ;
				}

				?>
			</div>

		</div>
	</header> <!-- end article header -->

	<div id="content">
	
		<div id="inner-content" class="row">
	
		    <main id="main" class="large-8 medium-8 columns" role="main">
				
				<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

			    	<?php get_template_part( 'parts/loop', 'page' ); ?>
			    
			    <?php endwhile; endif; ?>							
			    					
			</main> <!-- end #main -->

		    <?php get_sidebar(); ?>
		    
		</div> <!-- end #inner-content -->

	</div> <!-- end #content -->

<?php get_footer(); ?>