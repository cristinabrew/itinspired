 // Bodymovin' Animation

 jQuery(document).ready(function($) {
 var anim;
    var elem = document.getElementById('bodymovin');
    var animData = {
        container: elem,
        renderer: 'svg',
        loop: false,
        autoplay: true,
        rendererSettings: {
            progressiveLoad: false
        },
        path: '/wp-content/themes/ITinspired%20v1.0/assets/js/data.json'
    };
    anim = bodymovin.loadAnimation(animData);

    anim.addEventListener("DOMLoaded", function() {
        anim.play();
    });

});


//gets the current time. 

jQuery(document).ready(function($) {

var d = new Date();
if(d.getHours() >= 8 && d.getHours() <= 17 ){
    $(".open").show();
    $(".closed").hide();
}
else {  
     $(".closed").show();
    $(".open").hide();
}

});

jQuery(document).foundation();
// Show/Hide Function - Team Members
jQuery(document).ready(function(){
	jQuery(".toggled-row").hide();
    jQuery(".toggler").click(function() {
        jQuery('.toggled-'+this.id+'').toggle();
    		var anchor = $('#'+this.id+'');
    	});
});

// Show/Hide Function for Solutions
jQuery( '.popup-toggle' ).click(function() {
    jQuery( '.popup' ).addClass( 'hide' );
    var thePopup = jQuery( this ).data( 'popup' );
    jQuery( '#'+thePopup ).removeClass( 'hide' );
});

jQuery( '.popup-close-button' ).click(function() {
    jQuery( '.popup' ).addClass( 'hide' );
});

// AOS Show by Scroll
AOS.init();

//Initiate Grid
	$(function() {
		Grid.init();
	});
// Scroll to accordion top

// $("#solution-accordion").on("up.zf.accordion", function(event) {
//    setTimeout(function(){
//        $('html,body').animate({scrollTop: $('.is-active').offset().top}, 'slow');
//    }, 250); //Adjust to match slideSpeed
//});
/*
These functions make sure WordPress
and Foundation play nice together.
*/

jQuery(document).ready(function() {

    // Remove empty P tags created by WP inside of Accordion and Orbit
    jQuery('.accordion p:empty, .orbit p:empty').remove();

	 // Makes sure last grid item floats left
	jQuery('.archive-grid .columns').last().addClass( 'end' );

	// Adds Flex Video to YouTube and Vimeo Embeds
  jQuery('iframe[src*="youtube.com"], iframe[src*="vimeo.com"]').each(function() {
    if ( jQuery(this).innerWidth() / jQuery(this).innerHeight() > 1.5 ) {
      jQuery(this).wrap("<div class='widescreen flex-video'/>");
    } else {
      jQuery(this).wrap("<div class='flex-video'/>");
    }
  });

});
