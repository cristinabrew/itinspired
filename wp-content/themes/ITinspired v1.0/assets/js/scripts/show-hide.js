// Show/Hide Function - Team Members
jQuery(document).ready(function(){
	jQuery(".toggled-row").hide();
    jQuery(".toggler").click(function() {
        jQuery('.toggled-'+this.id+'').toggle();
    		var anchor = $('#'+this.id+'');
    	});
});

// Show/Hide Function for Solutions
jQuery( '.popup-toggle' ).click(function() {
    jQuery( '.popup' ).addClass( 'hide' );
    var thePopup = jQuery( this ).data( 'popup' );
    jQuery( '#'+thePopup ).removeClass( 'hide' );
});

jQuery( '.popup-close-button' ).click(function() {
    jQuery( '.popup' ).addClass( 'hide' );
});

// AOS Show by Scroll
AOS.init();

//Initiate Grid
	$(function() {
		Grid.init();
	});