 // Bodymovin' Animation

 jQuery(document).ready(function($) {
 var anim;
    var elem = document.getElementById('bodymovin');
    var animData = {
        container: elem,
        renderer: 'svg',
        loop: false,
        autoplay: true,
        rendererSettings: {
            progressiveLoad: false
        },
        path: '/wp-content/themes/ITinspired%20v1.0/assets/js/data.json'
    };
    anim = bodymovin.loadAnimation(animData);

    anim.addEventListener("DOMLoaded", function() {
        anim.play();
    });

});

