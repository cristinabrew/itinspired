<?php
// Related Posts Function, matches posts by tags - call using joints_related_posts(); )
function joints_related_posts() {
	global $post;
	$tag_arr = '';
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) {
			$tag_arr .= $tag->slug . ',';
		}
		$args = array(
			'tag' => $tag_arr,
			'numberposts' => 3, /* you can change this to show more */
			'post__not_in' => array($post->ID)
		);
		$related_posts = get_posts( $args );
		if($related_posts) {
		echo __( '<hr/><h4>Related Posts</h4>', 'jointswp' );
		echo '<div id="joints-related-posts" class="row archive-grid" data-equalizer>';
			foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
				<div class="large-4 medium-4 small-12 columns related-posts" data-equalizer-watch>
					<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article">
					
						<div class="featured-image" itemprop="articleBody">
							<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail('full', array('class' => 'image-hover')); ?></a>
						</div> <!-- end article section -->
					
						<header class="article-header-feed">
							<h5 class="title"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>	
							<?php get_template_part( 'parts/content', 'byline-date' ); ?>	

						</header> <!-- end article header -->	
										    							
					</article> <!-- end article -->
				</div>
			<?php endforeach; }
			}
	wp_reset_postdata();
	echo '</div>';
} /* end joints related posts function */