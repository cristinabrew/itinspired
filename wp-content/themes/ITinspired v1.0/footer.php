</div> <!-- end .wrap for sticky footer -->



				<footer class="footer" role="contentinfo">
					<div id="inner-footer" class="row">
						<div class="large-6 medium-6 small-12 columns">
							<nav role="navigation">
								<a href="<?php echo home_url(); ?>"><div class="footer-logo">
								</div></a>
								<p class="footer-contact">2354 S. Acadian Thruway, Suite B<br/>
								Baton Rouge, Louisiana 70808<br/>
								225 / 424 / 8000</p>
	    						
	    					</nav>
	    				</div>
						<div class="large-6 medium-6 small-12 columns">
							
							<div class="footer-links">
							<?php joints_footer_links(); ?>
						 	</div>
						 	<div class="footer-social-links">
							<ul class="social-links menu">
								<li><a href="http://www.facebook.com/ITinspired" target="_blank"><img src="<?php echo get_theme_file_uri( '/assets/images/facebook@2x.png' ); ?>"></a></li>
								<li><a href="https://www.linkedin.com/company/itinspired" target="_blank"><img src="<?php echo get_theme_file_uri( '/assets/images/linkedin@2x.png' ); ?>"></a></li>
								<li><a href="http://www.instagram.com/ITinspiredBR/" target="_blank"><img src="<?php echo get_theme_file_uri( '/assets/images/instagram@2x.png' ); ?>"></a></li>
							</ul>
							</div>
						</div>
					</div> <!-- end #inner-footer -->
				</footer> <!-- end .footer -->
			</div>  <!-- end .main-content -->
		</div> <!-- end .off-canvas-wrapper -->
		<?php wp_footer(); ?>
		<script type="text/javascript">
		    window._chatlio = window._chatlio||[];
		    !function(){ var t=document.getElementById("chatlio-widget-embed");if(t&&window.ChatlioReact&&_chatlio.init)return void _chatlio.init(t,ChatlioReact);for(var e=function(t){return function(){_chatlio.push([t].concat(arguments)) }},i=["configure","identify","track","show","hide","isShown","isOnline", "page"],a=0;a<i.length;a++)_chatlio[i[a]]||(_chatlio[i[a]]=e(i[a]));var n=document.createElement("script"),c=document.getElementsByTagName("script")[0];n.id="chatlio-widget-embed",n.src="https://w.chatlio.com/w.chatlio-widget.js",n.async=!0,n.setAttribute("data-embed-version","2.3");
		       n.setAttribute('data-widget-id','928a1e4b-bb39-4ff1-5a72-cd18dd346dcd');
		       c.parentNode.insertBefore(n,c);
		    }();
		</script>
	</body>
</html> <!-- end page -->