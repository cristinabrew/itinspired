<?php
/*
Template Name: Careers
*/
?>

<?php get_header(); ?>
	
	<header class="article-header careers-header" style="background-image: url(<?php the_field('background_image'); ?>);">
		<div class="row">
			<div class="large-6 medium-6 medium-push-6 columns"> 
				<p class="text-center"><img src="<?php the_field('header_icon'); ?>" alt="<?php the_title(); ?>" width="65%"></p>
			</div>
			<div class="large-6 medium-6 medium-pull-6 columns">
				<h2 class="page-subtitle"><?php the_field('page_subtitle'); ?></h2>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php if(get_field('header_intro'))
				{
					echo get_field('header_intro');
				}

				?>
				<a href="#current-openings" class="button hollow white" data-smooth-scroll>View Current Openings</a>
			</div>

		</div>
	</header> <!-- end article header -->

	<div id="content">
	
		<div id="careers-inner-content">

			<div class="careers-content-section">
					<div class="careers-content-section-img medium-push-6">
						<img src="<?php echo get_theme_file_uri( '/assets/images/love-where-you-work@2x.jpg' ); ?>" alt="" />
					</div>	

					<div class="careers-content-section-block medium-pull-6" >
						<h5>Love Where You Work</h5>
						<h3>We’re Doing it Our Way</h3>
						<p>We’re not your typical IT company. We put people first, and we aim to inspire with every interaction. With our squad, “work hard, play hard” is an understatement. We love the work we do and the people we get to work with. We’re changing the perception of IT with solutions and a smile. What’s not to love?</p> 
					</div>

			</div>
			
			<div class="why-itinspired">
		    	<div class="row">
			    	<div class="small-12 large-6 columns why-iti-content">
			    		<h3>Why ITinspired?</h3>
			    		<p>We believe it all starts with employees that feel valued and inspired. Only then can we serve with that same principle in mind – to create happy customers that feel appreciated and inspired.</p>
			    	</div>
			    	<div class="small-12 large-6 columns why-iti-img">
			    		<img src="<?php echo get_theme_file_uri( '/assets/images/why-iti-graphic@2x.png' ); ?>" alt="" />
			    	</div>
			    </div>
		    </div>

		    <div class="careers-content-section">

					<div class="careers-content-section-img">
						<img src="<?php echo get_theme_file_uri( '/assets/images/love-who-you-work-with@2x.jpg' ); ?>" alt="" />
					</div>

					<div class="careers-content-section-block">
						<h5>Love Who You Work With</h5>
						<h3>Meet Your New Squad</h3>
						<p>You spend as much time with your work family as your real family, so why not enjoy it?</p> 
						<a href="/squad" class="button hollow">Get to Know Our Team</a>
					</div>
				
			</div>

			<div class="what-we-give">
				<h3>What We Give</h3>

				<div class="row small-up-1 medium-up-2">
					<div class="column column-block what-we-give-content-block">
						<img class="avatar" src="<?php echo get_theme_file_uri( '/assets/images/benefits-icon@2x.png' ); ?>" alt="" />
						<div>
						<h4 class="benefits-title">Competitive Benefits</h4>
						<p class="benefits-details">Full-time and salaried employees enjoy paid health, vision, dental, short-term and long-term disability, life insurance, and retirement.</p>
						</div>
					</div>
					<div class="column column-block what-we-give-content-block">
						<img class="avatar" src="<?php echo get_theme_file_uri( '/assets/images/lagniappe-icon@2x.png' ); ?>" alt="" />
						<div>
						<h4 class="benefits-title">A Little Lagniappe</h4>
						<p class="benefits-details">Snacks, coffee, and lunch is on us (most of the time)! The coolest company swag, plus fun social events, nerf guns, and a DRONE!</p>
						</div>
					</div>
					<div class="column column-block what-we-give-content-block">
						<img class="avatar" src="<?php echo get_theme_file_uri( '/assets/images/connections-icon@2x.png' ); ?>" alt="" />
						<div>
						<h4 class="benefits-title">Community Connection</h4>
						<p class="benefits-details">We’re invested in giving back. We proudly support local bussinesses and organizations, charitable events, and the leaders of tomorrow. </p>
						</div>
					</div>
					<div class="column column-block what-we-give-content-block">
						<img class="avatar" src="<?php echo get_theme_file_uri( '/assets/images/learning-icon@2x.png' ); ?>" alt="" />
						<div>
						<h4 class="benefits-title">Continued Learning</h4>
						<p class="benefits-details">We’re not just an office - we’re a classroom. Our culture fosters a positive learning environment through training, certifications, personal growth, and professional development.</p>
						</div>
					</div>
				</div>

			</div>
<a name="current-openings"></a>
			
			   
			<div class="careers-content-section">

	    		<div class="careers-content-section-img medium-push-6">
					<img src="<?php echo get_theme_file_uri( '/assets/images/current-openings@2x.png' ); ?>" alt="" />
				</div>

		    	<div class="careers-content-section-block medium-pull-6">
		    		
		    		<h3 id="current-openings">Current Openings</h3>
					<p>Ready to join the most fun-loving tech squad around? Check out our current openings:</p>
					
				    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

							<?php get_template_part( 'parts/loop', 'page' ); ?>
									
						<?php endwhile; endif; ?>
						
						<?php get_sidebar('jobssb'); ?>
				</div>
			</div>

		
		   
		</div> <!-- End inner-content -->

	</div> <!-- End #content -->


<?php get_footer(); ?>