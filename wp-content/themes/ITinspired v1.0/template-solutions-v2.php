<?php
/*
Template Name: Solutions v2
*/
?>

<?php get_header(); ?>
	
	<header class="article-header" style="background-image: url(<?php the_field('background_image'); ?>);">
		<div class="row">
			<div class="small-12 medium-6 medium-push-6 columns"> 
				<p class="text-center"><img src="<?php the_field('header_icon'); ?>" alt="<?php the_title(); ?>" width="65%"></p>
			</div>
			<div class="small-12 medium-6 medium-pull-6 columns">
				<h2 class="page-subtitle"><?php the_field('page_subtitle'); ?></h2>
				<h1 class="page-title"><?php the_title(); ?></h1>
				<?php if(get_field('header_intro'))
				{
					echo get_field('header_intro') ;
				}

				?>
			</div>

		</div>
	</header> <!-- end article header -->
			
	<div id="content">
	
		    <div class="solutions-tab-section">
		    		<h3>Learn More About Our Solutions:</h3>
	    		
	    		<div class="tabs-container show-for-medium">
		    		<ul class="tabs" data-deep-link="true" data-deep-link-smudge="true" data-deep-link-smudge="5000" data-tabs id="solution-tabs">
					  <li class="tabs-title is-active"><a href="#data" aria-selected="true"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/data@2x.png' ); ?>">Data</a></li>
					  <li class="tabs-title"><a href="#cloud"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/cloud@2x.png' ); ?>">Cloud</a></li>
					  <li class="tabs-title"><a href="#voice"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/voice@2x.png' ); ?>">Voice</a></li>
					</ul>

					<div class="tabs-content" data-tabs-content="solution-tabs">
						 <div class="tabs-panel is-active" id="data">

						    <?php if( have_rows('data_bucket') ): ?>
						    <div class="row small-up-1 medium-up-2 large-up-2">
							    <?php while( have_rows('data_bucket') ): the_row(); ?>
								    <?php 
							        
							        // vars
							        $title = get_sub_field('data_title'); 
							        $description = get_sub_field('data_description'); 
							       
							        
							        ?>
								    <div class="column column-block"> 
									    <h5><?php echo $title; ?></h5>
										<p><?php echo $description; ?></p>  
								    </div>
							    <?php endwhile; ?>
							 
							   </div>
							 
							<?php endif; ?>
				
						  </div>
						  <div class="tabs-panel" id="cloud">
						   	<?php if( have_rows('cloud_bucket') ): ?>
						    <div class="row small-up-1 medium-up-2 large-up-2">
							    <?php while( have_rows('cloud_bucket') ): the_row(); ?>
								    <?php 
							        
							        // vars
							        $title = get_sub_field('cloud_title'); 
							        $description = get_sub_field('cloud_description'); 
							       
							        
							        ?>
								    <div class="column column-block"> 
									    <h5><?php echo $title; ?></h5>
										<p><?php echo $description; ?></p>  
								    </div>
							    <?php endwhile; ?>
							 
							   </div>
							 
							<?php endif; ?>
						  </div>
						  <div class="tabs-panel" id="voice">
						    <?php if( have_rows('voice_bucket') ): ?>
						    <div class="row small-up-1 medium-up-2 large-up-2">
							    <?php while( have_rows('voice_bucket') ): the_row(); ?>
								    <?php 
							        
							        // vars
							        $title = get_sub_field('voice_title'); 
							        $description = get_sub_field('voice_description'); 
							       
							        
							        ?>
								    <div class="column column-block"> 
									    <h5><?php echo $title; ?></h5>
										<p><?php echo $description; ?></p>  
								    </div>
							    <?php endwhile; ?>
							 
							</div>
							 
							<?php endif; ?>
						  </div>
			    	</div> <!-- End .tabs-content -->
			    </div> <!-- End .tabs-container for large screens -->

			    <div class="accordion-container hide-for-medium">
			    	<ul class="accordion" data-accordion data-allow-all-closed="true" data-deep-link="true" data-update-history="true" id="solution-accordion">
			    		<li class="accordion-item" data-accordion-item>
			    			<a href="#data" class="accordion-title"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/data@2x.png' ); ?>">Data</a>
						    <div class="accordion-content" data-tab-content id="data">
						     	<?php if( have_rows('data_bucket') ): ?>
								    <div class="row small-up-1 medium-up-2 large-up-2">
									    <?php while( have_rows('data_bucket') ): the_row(); ?>
										    <?php 
									        
									        // vars
									        $title = get_sub_field('data_title'); 
									        $description = get_sub_field('data_description'); 
									       
									        
									        ?>
										    <div class="column column-block"> 
											    <h5><?php echo $title; ?></h5>
												<p><?php echo $description; ?></p>  
										    </div>
									    <?php endwhile; ?>
									 
									   </div>
									 
									<?php endif; ?>
						    </div>
			    		</li>
			    		<li class="accordion-item" data-accordion-item>
			    			<a href="#cloud" class="accordion-title"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/cloud@2x.png' ); ?>">Cloud</a>
						    <div class="accordion-content" data-tab-content id="cloud">
						     	<?php if( have_rows('cloud_bucket') ): ?>
								    <div class="row small-up-1 medium-up-2 large-up-2">
									    <?php while( have_rows('cloud_bucket') ): the_row(); ?>
										    <?php 
									        
									        // vars
									        $title = get_sub_field('cloud_title'); 
									        $description = get_sub_field('cloud_description'); 
									       
									        
									        ?>
										    <div class="column column-block"> 
											    <h5><?php echo $title; ?></h5>
												<p><?php echo $description; ?></p>  
										    </div>
									    <?php endwhile; ?>
									 
									   </div>
									 
									<?php endif; ?>
						    </div>
			    		</li>
			    		<li class="accordion-item" data-accordion-item>
			    			<a href="#voice" class="accordion-title"><img class="service-button-icons" src="<?php echo get_theme_file_uri( '/assets/images/voice@2x.png' ); ?>">Voice</a>
						    <div class="accordion-content" data-tab-content id="voice">
						     	<?php if( have_rows('voice_bucket') ): ?>
								    <div class="row small-up-1 medium-up-2 large-up-2">
									    <?php while( have_rows('voice_bucket') ): the_row(); ?>
										    <?php 
									        
									        // vars
									        $title = get_sub_field('voice_title'); 
									        $description = get_sub_field('voice_description'); 
									       
									        
									        ?>
										    <div class="column column-block"> 
											    <h5><?php echo $title; ?></h5>
												<p><?php echo $description; ?></p>  
										    </div>
									    <?php endwhile; ?>
									 
									   </div>
									 
									<?php endif; ?>
						    </div>
			    		</li>
			    	</ul>
			    </div>
			</div> <!-- End .solutions-tab-section -->



		    	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<p class="lead"><?php get_template_part( 'parts/loop', 'page' ); ?></p>
							
				<?php endwhile; endif; ?>


				<div id="partners" class="row">
				    <div class="large-12 medium-12 columns">
					    <h5>The Company We Keep</h5>
					    <h3>Partners</h3>

					    <?php if( have_rows('partner') ): ?>

					    <div class="row small-up-2 medium-up-5 large-up-5" data-aos="fade-up">
						    <?php while( have_rows('partner') ): the_row(); 
							    
						        
						        // vars
						        $plogo = get_sub_field('partner_logo'); 
						        $ptitle = get_sub_field('partner_title'); 
						        $pdescription = get_sub_field('partner_description'); 
						        
						        ?>

							    <div class="column column-block"> 
							    	
							    	

							    	<ul class="accordion" data-accordion data-allow-all-closed="true">
							    		<li class="accordion-item" data-accordion-item>
											<a href="#" class="accordion-title"><img class="partner-logo" src="<?php echo $plogo; ?>" alt="<?php echo $ptitle; ?>">
											</a>
							    		<div class="accordion-content" data-tab-content>
												<p><?php echo $pdescription; ?></p>  
										</div>
										</li>
							    	</ul>

									
							    </div>
						    <?php endwhile; ?>
						 
						</div>
						 
						<?php endif; ?>
					</div>
				</div>
				

				<div class="testimonials solutions-footer">
					<div class="row">
					<?php get_sidebar('testimonialsb'); ?>
					</div>
				</div>	


			<!-- </main> --> <!-- end #main -->
		    
	<!-- 	</div>  --><!-- end #inner-content -->
	
	</div> <!-- end #content -->

<?php get_footer(); ?>
